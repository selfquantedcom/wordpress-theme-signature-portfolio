<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '6zGlayBRqDPwjqilYDpDbHQilze4MbcMLmcUih5sD/tiZjc/M9cmktwEousib3W3ed0L7vT3r6AQcdTR1D5RUg==');
define('SECURE_AUTH_KEY',  '7mDGt7QCWwO4jc/zriEEJHsyu3JvO2grmWgFO8N09fy4SEOKJrBbzW08n/UNuNxVRI4agvySJX9lhzlYQCVSNA==');
define('LOGGED_IN_KEY',    'Jw7uJSygbhKsDvVgbqdIYjfMvASR1RdA1lVsQaUKYfwgzc4NbrcHGXh0DeCiKyGhcJZqbM/QT9JTeT06Un5mGQ==');
define('NONCE_KEY',        'XUKVJmGcqFyry40m9fivgCrjZM5+yEbIRwk6Z3zhlhLewYlg9UufVii79Vnz52nrDfO5DqNtZA4eGmQm1Kh9xw==');
define('AUTH_SALT',        'VS0S33yYE4Qy4V4t4l2tsLA3bCtsoYVaOwXNT6dsCMxKVy7QijTRFxYfZTtLKWIZFjWT76jw4EDmaNbWuMLDyg==');
define('SECURE_AUTH_SALT', 't3xlC9QNLUTwV0IDo1YC2LlETo6v/WIlQJiYXdU5O2l3xTVv4g6ei3NGTuhZv3iTjUOLNcYZOjdaV8qb5tD4Qg==');
define('LOGGED_IN_SALT',   'nqtAiHNHNiQwT7NaHvG7LNbVcVlqxvT8MRcrwgk23w/UbY1EsX84YiqVz9R8n9dHZb/E0M1XMqEISu/DNEJDiA==');
define('NONCE_SALT',       '6u5Hx2sN+41dYEAPFRMkPapIRpfHlnxz08ffnWkp5JDhIvCVadwFzgr10XmzGrbgh0AJSrVYmmMgnOrCSvb+pQ==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
