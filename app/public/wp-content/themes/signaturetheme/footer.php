
<footer class='navbar navbar-expand-sm bg-color-dark navbar-dark mt-5'>
    <div class="container text-center justify-content-center m">
        <div class="row full-width mt-4">            

            <div class="row justify-content-center full-width social-media-links mt-2 mb-4">
                <div class="col-12">
                    <ul class="list-inline centered-content">

                        <li class="list-inline-item">
                            <a href='#' target="_blank" class='btn btn-outline-light btn-social text-center rounded-circle'><i class="fa fa-linkedin"></i></a>
                        </li>

                        <li class="list-inline-item">
                            <a href='#' target="_blank" class='btn btn-outline-light btn-social text-center rounded-circle'><i class="fa fa-bitbucket"></i></a>
                        </li>

                    </ul>
                </div>
            </div>

        </div>
    </div>
</footer>