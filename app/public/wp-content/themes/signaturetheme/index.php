<?php get_header(); ?>

<!-- User description -->
<div id="about" class="jumbotron mb-0 user-introduction">

    <div class="row">

        <div class="col-12 mb-2 text-center">
            <img src="<?php echo esc_url(get_theme_mod('user_image')) ?>" alt="user profile image" class="img-responsive img-fluid rounded-circle centered-content max-width-200 mb-2" />
            <h3 class="mb-2"><?php echo get_theme_mod('user_name'); ?></h3>
            <p><strong><?php echo get_theme_mod('user_occupation'); ?></strong></p>
            <p><i><?php echo get_theme_mod('user_power_statement'); ?></i></p>
            <a href="https://cdn.shopify.com/s/files/1/0289/2844/2420/files/Frantisek_Friedl_CV.pdf?v=1618952392" target='_blank' class="text-underline"><img src="<?php echo get_theme_file_uri('/images/resume_icon.png') ?>" alt="" width="100px">
                <p>View Resume</p>
            </a>

            <ul class="list-inline centered-content">

                <li class="list-inline-item">
                    <a href='#' target="_blank" class='btn btn-outline-light btn-social text-center rounded-circle'><i class="fa fa-linkedin"></i></a>
                </li>

                <li class="list-inline-item">
                    <a href='#' target="_blank" class='btn btn-outline-light btn-social text-center rounded-circle'><i class="fa fa-bitbucket"></i></a>
                </li>

            </ul>
        </div>
    </div>

</div>

<div id="qualifications" class="jumbotron mb-5">
    <div class="row justify-content-center">
        <div class="col-xs-10 col-sm-10 col-md-6 col-lg-6 text-center">
            <h3>Skills & Education</h3>
        </div>
    </div>
</div>

<div class="container">

    <!-- Professional Experience - Qualifications -->
    <div class="row mb-5">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex">
                        <div class="d-inline-block">
                            <h3 class="mb-0 line-50"><?php echo get_theme_mod('education_skills_section_card_title'); ?></h3>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row ">
                        <div class="col-12 col-lg-6 order-12 order-lg-0 my-auto">
                            <img src="<?php echo esc_url(get_theme_mod('skills_image')) ?>" alt="" class="d-block img-fluid centered-content max-width-350 mt-2">
                        </div>
                        <div class="col-12 col-lg-6 my-auto">
                            <div class="row">
                                <div class="col-12 text-left">
                                    <h3><?php echo get_theme_mod('education_skills_section_card_item1_title'); ?></h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 text-left concise-text">
                                    <p><?php echo get_theme_mod('professional_experience_card_1_project_description_1'); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row ">
                        <div class="col-12 col-lg-6 order-12 order-lg-1 my-auto">
                            <img src="<?php echo esc_url(get_theme_mod('education_image')) ?>" alt="" class="d-block img-fluid centered-content max-width-350 mt-2">
                        </div>
                        <div class="col-12 col-lg-6 initiatives my-auto">
                            <div class="row">
                                <div class="col-12 text-left">
                                    <h3><?php echo get_theme_mod('education_skills_section_card_item2_title'); ?></h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 text-left concise-text">
                                    <p><?php echo get_theme_mod('professional_experience_card_1_project_description_1'); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Certifications Carousel -->
    <div class="row mb-5">
        <div id="carousel-qualifications" class="carousel slide col-12 col-md-6" data-ride="carousel">
            <div class="carousel-inner" role="listbox">

                <!-- Dynamically create up to 3 certifications -->
                <div class="carousel-item carousel-img active mb-2">
                    <div class="col-12">
                        <img src="<?php echo esc_url(get_theme_mod('certificate_1')) ?>" alt="" class="d-block img-fluid centered-content max-width-200">
                    </div>
                </div>

                <div class="carousel-item carousel-img mb-2">
                    <div class="col-12">
                        <img src="<?php echo esc_url(get_theme_mod('certificate_2')) ?>" alt="certificate" class="d-block img-fluid centered-content max-width-200">
                    </div>
                </div>

                <div class="carousel-item carousel-img mb-2">
                    <div class="col-12">
                        <img src="<?php echo esc_url(get_theme_mod('certificate_3')) ?>" alt="certificate" class="d-block img-fluid centered-content max-width-200">
                    </div>

                </div>

                <a class="carousel-control-prev next-arrow1" href="#carousel-qualifications" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next next-arrow1" href="#carousel-qualifications" role="button" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </a>

            </div>
        </div>

        <div class="col-12 col-md-6 my-auto">
            <div class="row">
                <div class="col-12 text-left center-text-mobile">
                    <h2><?php echo get_theme_mod('education_skills_section_certifications_title'); ?> </h2>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-left concise-text center-text-mobile row-carousel-text">
                    <p><?php echo get_theme_mod('education_skills_section_certifications_description'); ?> </p>

                </div>
            </div>
        </div>

    </div>
</div>
<!-- Professional Experience -->
<div class="jumbotron mb-5">

    <div class="row">
        <div class="col-12 text-center">
            <h3><?php echo get_theme_mod('professional_experience_title'); ?></h3>
        </div>
    </div>

</div>

<div class="container">

    <!-- Professional Experience -->
    <div class="row mb-5">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex">
                        <div class="d-inline-block">
                            <h3 class="mb-0 line-50"><?php echo get_theme_mod('professional_experience_card_title_1'); ?> </h3>
                        </div>
                        <div class="d-inline-block ml-auto">
                            <img src="<?php echo esc_url(get_theme_mod('professional_experience_card_logo_1')) ?>" alt="" height="50" class="">
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row ">
                        <div class="col-12 my-auto">
                            <div class="row">
                                <div class="col-12 text-left concise-text">
                                    <p><?php echo get_theme_mod('professional_experience_card_1_project_description_1'); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row ">
                        <div class="col-12 col-lg-6 order-12 order-lg-1 my-auto">
                            <img src="<?php echo esc_url(get_theme_mod('project_1')) ?>" alt="" class="d-block img-fluid centered-content max-width-350 mt-2">
                        </div>
                        <div class="col-12 col-lg-6 initiatives my-auto">
                            <div class="row">
                                <div class="col-12 text-left">
                                    <h3><?php echo get_theme_mod('professional_experience_card_1_project_title_1'); ?></h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 text-left concise-text">
                                    <p><?php echo get_theme_mod('professional_experience_card_1_project_description_1'); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row ">
                        <div class="col-12 col-lg-6 my-auto">
                            <img src="<?php echo esc_url(get_theme_mod('project_2')) ?>" alt="" class="d-block img-fluid centered-content max-width-350 mt-2">
                        </div>
                        <div class="col-12 col-lg-6 initiatives my-auto">
                            <div class="row">
                                <div class="col-12 text-left">
                                    <h3><?php echo get_theme_mod('professional_experience_card_1_project_title_2'); ?></h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 text-left concise-text">
                                    <p><?php echo get_theme_mod('professional_experience_card_1_project_description_1'); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row ">
                        <div class="col-12 col-lg-6 order-12 order-lg-1 my-auto">
                            <img src="<?php echo esc_url(get_theme_mod('project_3')) ?>" alt="" class="d-block img-fluid centered-content max-width-350 mt-2">
                        </div>
                        <div class="col-12 col-lg-6 initiatives my-auto">
                            <div class="row">
                                <div class="col-12 text-left">
                                    <h3><?php echo get_theme_mod('professional_experience_card_1_project_title_3'); ?></h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 text-left concise-text">
                                    <p><?php echo get_theme_mod('professional_experience_card_1_project_description_1'); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Colegue Testimonials Carousel -->
    <div class="row justify-content-center text-center mb-5">
        <div class="col-12">
            <div id="carousel-testimonials-1" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" role="listbox">

                    <div class="carousel-item active">
                        <div class="row justify-content-center text-center mb-5 my-auto">

                            <div class="col-12 col-lg-3">
                                <div class="row justify-content-center text-center">
                                    <img src="<?php echo esc_url(get_theme_mod('employer_testimonials_1_image_1')) ?>" alt="" class="d-block img-fluid rounded-circle centered-content max-width-200 mb-2">
                                </div>
                            </div>

                            <div class="col-12 col-lg-6 my-auto">
                                <div class="row">
                                    <div class="col-12 concise-text">
                                        <p><i>"<?php echo get_theme_mod('employer_testimonials_1_quote_1'); ?>"</i></p>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-12 testimonial-client text-color">
                                        <p><?php echo get_theme_mod('employer_testimonials_1_referee_name_1'); ?></p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="row justify-content-center text-center mb-5 my-auto">

                            <div class="col-12 col-lg-3">
                                <div class="row justify-content-center text-center">
                                    <img src="<?php echo esc_url(get_theme_mod('employer_testimonials_1_image_2')) ?>" alt="" class="d-block img-fluid rounded-circle centered-content max-width-200 mb-2">
                                </div>
                            </div>

                            <div class="col-12 col-lg-6 my-auto">
                                <div class="row">
                                    <div class="col-12 concise-text">
                                        <p>"<?php echo get_theme_mod('employer_testimonials_1_quote_2'); ?>"</p>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-12 testimonial-client text-color">
                                        <p><?php echo get_theme_mod('employer_testimonials_1_referee_name_2'); ?></p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="row justify-content-center text-center mb-5 my-auto">

                            <div class="col-12 col-lg-3">
                                <div class="row justify-content-center text-center">
                                    <img src="<?php echo esc_url(get_theme_mod('employer_testimonials_1_image_3')) ?>" alt="" class="d-block img-fluid rounded-circle centered-content max-width-200 mb-2">
                                </div>
                            </div>

                            <div class="col-12 col-lg-6 my-auto">
                                <div class="row">
                                    <div class="col-12 concise-text">
                                        <p><i>"<?php echo get_theme_mod('employer_testimonials_1_quote_3'); ?>"</i></p>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-12 testimonial-client text-color">
                                        <p><?php echo get_theme_mod('employer_testimonials_1_referee_name_3'); ?></p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <a class="carousel-control-prev next-arrow1" href="#carousel-testimonials-1" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon"></span>
                    </a>
                    <a class="carousel-control-next next-arrow1" href="#carousel-testimonials-1" role="button" data-slide="next">
                        <span class="carousel-control-next-icon"></span>
                    </a>

                </div>
            </div>
        </div>
    </div>

    <!-- Professional Experience -->
    <div class="row mb-5">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex">
                        <div class="d-inline-block">
                            <h3 class="mb-0 line-50"><?php echo get_theme_mod('professional_experience_card_title_2'); ?> </h3>
                        </div>
                        <div class="d-inline-block ml-auto">
                            <img src="<?php echo esc_url(get_theme_mod('professional_experience_card_logo_2')) ?>" alt="" height="50" class="">
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row ">
                        <div class="col-12 initiatives my-auto">
                            <div class="row">
                                <div class="col-12 text-left concise-text">
                                    <p><?php echo get_theme_mod('professional_experience_card_1_project_description_1'); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row ">
                        <div class="col-12 col-lg-6 order-12 order-lg-1 my-auto">
                            <img src="<?php echo esc_url(get_theme_mod('project_4')) ?>" alt="" class="d-block img-fluid centered-content max-width-350 mt-2">
                        </div>
                        <div class="col-12 col-lg-6 initiatives my-auto">
                            <div class="row">
                                <div class="col-12 text-left">
                                    <h3><?php echo get_theme_mod('professional_experience_card_2_project_title_1'); ?></h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 text-left concise-text">
                                    <p><?php echo get_theme_mod('professional_experience_card_1_project_description_1'); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row ">
                        <div class="col-12 col-lg-6 my-auto">
                            <img src="<?php echo esc_url(get_theme_mod('project_5')) ?>" alt="" class="d-block img-fluid centered-content max-width-350 mt-2">
                        </div>
                        <div class="col-12 col-lg-6 initiatives my-auto">
                            <div class="row">
                                <div class="col-12 text-left">
                                    <h3><?php echo get_theme_mod('professional_experience_card_2_project_title_2'); ?></h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 text-left concise-text">
                                    <p><?php echo get_theme_mod('professional_experience_card_1_project_description_1'); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row ">
                        <div class="col-12 col-lg-6 order-12 order-lg-1 my-auto">
                            <img src="<?php echo esc_url(get_theme_mod('project_6')) ?>" alt="" class="d-block img-fluid centered-content max-width-350 mt-2">
                        </div>
                        <div class="col-12 col-lg-6 initiatives my-auto">
                            <div class="row">
                                <div class="col-12 text-left">
                                    <h3><?php echo get_theme_mod('professional_experience_card_2_project_title_3'); ?></h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 text-left concise-text">
                                    <p><?php echo get_theme_mod('professional_experience_card_1_project_description_1'); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Colegue Testimonials Carousel -->
    <div class="row justify-content-center text-center mb-5">
        <div class="col-12">
            <div id="carousel-testimonials-2" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" role="listbox">

                    <div class="carousel-item active">
                        <div class="row justify-content-center text-center mb-5 my-auto">

                            <div class="col-12 col-lg-3">
                                <div class="row justify-content-center text-center">
                                    <img src="<?php echo esc_url(get_theme_mod('employer_testimonials_2_image_1')) ?>" alt="" class="d-block img-fluid rounded-circle max-width-200 mb-2">
                                </div>
                            </div>

                            <div class="col-12 col-lg-6 my-auto">
                                <div class="row">
                                    <div class="col-12 concise-text">
                                        <p><i>"<?php echo get_theme_mod('employer_testimonials_2_quote_1'); ?>"</i></p>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-12 testimonial-client text-color">
                                        <p><?php echo get_theme_mod('employer_testimonials_2_referee_name_1'); ?></p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="row justify-content-center text-center mb-5 my-auto">

                            <div class="col-12 col-lg-3">
                                <div class="row justify-content-center text-center">
                                    <img src="<?php echo esc_url(get_theme_mod('employer_testimonials_2_image_2')) ?>" alt="" class="d-block img-fluid rounded-circle max-width-200 mb-2">
                                </div>
                            </div>

                            <div class="col-12 col-lg-6 my-auto">
                                <div class="row">
                                    <div class="col-12 concise-text">
                                        <p><i>"<?php echo get_theme_mod('employer_testimonials_2_quote_2'); ?>"</i></p>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-12 testimonial-client text-color">
                                        <p><?php echo get_theme_mod('employer_testimonials_2_referee_name_2'); ?></p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="row justify-content-center text-center mb-5 my-auto">

                            <div class="col-12 col-lg-3">
                                <div class="row justify-content-center text-center">
                                    <img src="<?php echo esc_url(get_theme_mod('employer_testimonials_2_image_3')) ?>" alt="" class="d-block img-fluid rounded-circle centered-content max-width-200 mb-2">
                                </div>
                            </div>

                            <div class="col-12 col-lg-6 my-auto">
                                <div class="row">
                                    <div class="col-12 concise-text">
                                        <p><i>"<?php echo get_theme_mod('employer_testimonials_2_quote_3'); ?>"</i></p>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-12 testimonial-client text-color">
                                        <p><?php echo get_theme_mod('employer_testimonials_2_referee_name_3'); ?></p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <a class="carousel-control-prev next-arrow1" href="#carousel-testimonials-2" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon"></span>
                    </a>
                    <a class="carousel-control-next next-arrow1" href="#carousel-testimonials-2" role="button" data-slide="next">
                        <span class="carousel-control-next-icon"></span>
                    </a>

                </div>
            </div>
        </div>
    </div>

</div>


<!-- Contact Form introduction -->
<div id="contact" class="jumbotron mb-5">
    <div class="row justify-content-center">
        <div class="col-xs-10 col-sm-10 col-md-6 col-lg-6 text-center">
            <h3>Contact</h3>
        </div>
    </div>
</div>

<div class="container">

    <!-- Contact Form -->
    <div class="card mt-5">
        <h5 class="card-header">Contact</h5>
        <div class="card-body">

            <form action="contact">
                <div class="form-group row">

                    <div class="col-12">
                        <label for="name">Name</label>
                        <input type="text" required class="form-control">
                    </div>

                    <div class="col-12">
                        <label for="email" class="mt-2">Email</label>
                        <input type="email" required class="form-control">
                    </div>

                    <div class="col-12">
                        <label for="comment" class="mt-2">Message</label>
                        <textarea name="comment" id="" cols="30" rows="10" class="form-control"></textarea>
                    </div>

                    <div class="col-12 mt-2">
                        <button type="submit" class="btn text-color-light">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>

<?php get_footer(); ?>