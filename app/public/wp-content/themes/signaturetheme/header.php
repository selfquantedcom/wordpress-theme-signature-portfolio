<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <header class="site-header">

        <nav id="top-navigation" class="top-navigation navbar navbar-expand-md bg-color-dark navbar-dark fixed-top">
            <div class="navbar-brand mr-auto"><a href="/"><img src="" alt=""></a></div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="collapsibleNavbar">
                <ul class="navbar-nav bg-color-dark ">

                    <li class="nav-item">
                        <a href="#about" class="nav-link text-color-light">About</a>
                    </li>

                    <li class="nav-item">
                        <a href="#qualifications" class="nav-link text-color-light">Qualifications</a>
                    </li>

                    <li class="nav-item">
                        <a href="#contact" class="nav-link text-color-light">Contact</a>
                    </li>

                </ul>
            </div>
        </nav>


    </header>