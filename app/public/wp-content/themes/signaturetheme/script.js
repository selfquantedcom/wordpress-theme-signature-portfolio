// Smooth Scrolling
$(document).ready(function(){
    $('#top-navigation a').on('click', function (e) {
        if (this.hash !== '') {
            e.preventDefault();
            const hash = this.hash;
            $('html, body').animate(
                {
                    scrollTop: $(hash).offset().top - 50,
                },
                800
            );
        }
    });
 });