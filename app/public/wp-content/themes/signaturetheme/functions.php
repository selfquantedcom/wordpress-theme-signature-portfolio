<?php

function theme_files() {
    wp_enqueue_script('jQuery', '//code.jquery.com/jquery-3.5.1.min.js', null, null, false);    
    wp_enqueue_script('bootstrap-js', '//stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js', false, null);    
    wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
    wp_enqueue_script( 'script', get_theme_file_uri('script.js'), array(), '1.0', false);    
    wp_enqueue_style('bootstrap', '//stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css');
    wp_enqueue_style('university_main_styles', get_theme_file_uri('/style.css'));  
    
}

function theme_features() {
  add_theme_support('title-tag');
}

add_action('wp_enqueue_scripts', 'theme_files');
add_action('after_setup_theme', 'theme_features');


function theme_customize_register( $wp_customize ) {
  
  // --- Theme Settings Section ---
  $wp_customize->add_section( 'theme_settings_section' , array(
    'title'      =>  'Theme Settings', 
    'priority'   => 1,
    'description' => 'Theme Settings'
  ));

  // Color Scheme - Darkest Color
  $wp_customize->add_setting( 'color_scheme_dark' , array(
    'default'   => '',
    'transport' => 'refresh'
  ));

  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'color_scheme_dark_control', array(
    'label'      => ('Darkest Color'),
    'section'    => 'theme_settings_section',
    'settings'   => 'color_scheme_dark',
    'width'      => '250'
  )));

  // Color Scheme - CTA Color
  $wp_customize->add_setting( 'color_scheme_cta' , array(
    'default'   => '',
    'transport' => 'refresh'
  ));

  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'color_scheme_cta_control', array(
    'label'      => ('CTA Color'),
    'section'    => 'theme_settings_section',
    'settings'   => 'color_scheme_cta',
    'width'      => '250'
  )));

  // Color Scheme - Accent Color
  $wp_customize->add_setting( 'color_scheme_accent' , array(
    'default'   => '',
    'transport' => 'refresh'
  ));

  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'color_scheme_accent_control', array(
    'label'      => ('Accent Color'),
    'section'    => 'theme_settings_section',
    'settings'   => 'color_scheme_accent',
    'width'      => '250'
  )));

   // Color Scheme - Lightest Color
   $wp_customize->add_setting( 'color_scheme_lightest' , array(
    'default'   => '',
    'transport' => 'refresh'
  ));

  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'color_scheme_lightest_control', array(
    'label'      => ('Lightest Color'),
    'section'    => 'theme_settings_section',
    'settings'   => 'color_scheme_lightest',
    'width'      => '250'
  )));

  // --- User Introduction Section ---
  $wp_customize->add_section( 'user_introduction_section' , array(
    'title'      =>  'User Introduction', 
    'priority'   => 2,
    'description' => 'User Introduction'
  ));

  // User Image
  $wp_customize->add_setting( 'user_image' , array(
    'default'   => '',
    'transport' => 'refresh'
  ));

  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'user_image_control', array(
    'label'      => ('Profile picture'),
    'section'    => 'user_introduction_section',
    'settings'   => 'user_image',
    'width'      => '250'
  )));

  // User name
  $wp_customize->add_setting( 'user_name' , array(
    'default'   => 'Name Surname',
    'transport' => 'refresh'
  ));

  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'user_name_control', array(
    'label'      => ('User Name'),
    'section'    => 'user_introduction_section',
    'settings'   => 'user_name',
    'type' => 'text'    
  )));

  // Occupation
  $wp_customize->add_setting( 'user_occupation' , array(
    'default'   => 'CEO of my life',
    'transport' => 'refresh'
  ));

  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'user_occupation_control', array(
    'label'      => ('What is your profession?'),
    'section'    => 'user_introduction_section',
    'settings'   => 'user_occupation',
    'type' => 'text'    
  )));

  // Power Statement
  $wp_customize->add_setting( 'user_power_statement' , array(
    'default'   => 'Skilled, hardworking and commited person.',
    'transport' => 'refresh'
  ));

  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'user_power_statement_control', array(
    'label'      => ('What are your main characteristics?'),
    'section'    => 'user_introduction_section',
    'settings'   => 'user_power_statement',
    'type' => 'text'    
  )));


  // --- Skills & Education Section ---
  $wp_customize->add_section( 'education_skills_section' , array(
    'title'      =>  'Skills & Education', 
    'priority'   => 2,
    'description' => 'Skills & Education'
  ));


  // Card Title
  $wp_customize->add_setting( 'education_skills_section_card_title' , array(
    'default'   => 'Qualifications',
    'transport' => 'refresh'
  ));
  
  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'education_skills_section_card_title_control', array(
    'label'      => ('Card Title'),
    'section'    => 'education_skills_section',
    'settings'   => 'education_skills_section_card_title',
    'type' => 'text'    
  )));

  // Skills Title
  $wp_customize->add_setting( 'education_skills_section_card_item1_title' , array(
    'default'   => 'Technical Skills',
    'transport' => 'refresh'
  ));
  
  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'education_skills_section_card_item1_title_control', array(
    'label'      => ('Skills - Title'),
    'section'    => 'education_skills_section',
    'settings'   => 'education_skills_section_card_item1_title',
    'type' => 'text'    
  )));

  // Skills Description
  $wp_customize->add_setting( 'skills_description' , array(
    'default'   => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum est debitis esse delectus. Accusamus nesciunt vel harum hic reiciendis eveniet aut laboriosam iste minima perspiciatis eaque et, nisi deleniti eum perferendis consequuntur sapiente neque quo consectetur ipsam exercitationem! Minus, praesentium assumenda itaque corrupti modi commodi reprehenderit maxime aliquid temporibus eligendi.',
    'transport' => 'refresh'
  ));

  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'skills_description_control', array(
    'label'      => ('What are your skills?'),
    'section'    => 'education_skills_section',
    'settings'   => 'skills_description',
    'type' => 'textarea'    
  )));

  // Skills Image      
  $wp_customize->add_setting( 'skills_image' , array(
    'default'   => '',
    'transport' => 'refresh'
  ));

  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'skills_image_control', array(
    'label'      => ('Skills - Image'),
    'section'    => 'education_skills_section',
    'settings'   => 'skills_image',
    'width'      => '250'
  )));    
    
    // Education Title
    $wp_customize->add_setting( 'education_skills_section_card_item2_title' , array(
      'default'   => 'Technical Skills',
      'transport' => 'refresh'
    ));
    
    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'education_skills_section_card_item2_title_control', array(
      'label'      => ('Education - Title'),
      'section'    => 'education_skills_section',
      'settings'   => 'education_skills_section_card_item2_title',
      'type' => 'text'    
    )));

   // Education Description
   $wp_customize->add_setting( 'education_description' , array(
    'default'   => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Error numquam excepturi optio quo sint quisquam libero alias labore molestias dignissimos. Voluptas recusandae quas eaque ipsum laboriosam quod esse doloribus necessitatibus ex itaque! Quisquam veritatis rem nulla, suscipit consequuntur ipsum expedita ratione minima minus unde dolore similique autem vel soluta tempore.',
    'transport' => 'refresh'
  ));

  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'education_description_control', array(
    'label'      => ('What is your education history?'),
    'section'    => 'education_skills_section',
    'settings'   => 'education_description',
    'type' => 'textarea'    
  )));

  // Educations Image      
  $wp_customize->add_setting( 'education_image' , array(
    'default'   => '',
    'transport' => 'refresh'
  ));

  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'education_image_control', array(
    'label'      => ('Education - Image'),
    'section'    => 'education_skills_section',
    'settings'   => 'education_image',
    'width'      => '250'
  )));  

  // Certifications Title
  $wp_customize->add_setting( 'education_skills_section_certifications_title' , array(
    'default'   => 'My Certifications',
    'transport' => 'refresh'
  ));
  
  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'education_skills_section_certifications_title_control', array(
    'label'      => ('Certifications - Title'),
    'section'    => 'education_skills_section',
    'settings'   => 'education_skills_section_certifications_title',
    'type' => 'text'    
  )));

   // Certifications - Description
   $wp_customize->add_setting( 'education_skills_section_certifications_description' , array(
    'default'   => 'I am certified in the following areas ... ',
    'transport' => 'refresh'
  ));
  
  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'education_skills_section_certifications_description_control', array(
    'label'      => ('Certifications - Description'),
    'section'    => 'education_skills_section',
    'settings'   => 'education_skills_section_certifications_description',
    'type' => 'text'    
  )));

  // Certificate 1 
  $wp_customize->add_setting( 'certificate_1' , array(
    'default'   => '',
    'transport' => 'refresh'
  ));
  
  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'certificate_1_control', array(
    'label'      => ('Your Certificate'),
    'section'    => 'education_skills_section',
    'settings'   => 'certificate_1'    
  )));  

    // Certificate 2 
    $wp_customize->add_setting( 'certificate_2' , array(
      'default'   => '',
      'transport' => 'refresh'
    ));
    
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'certificate_2_control', array(
      'label'      => ('Your Certificate'),
      'section'    => 'education_skills_section',
      'settings'   => 'certificate_2',
      'width'      => '450'
    )));  
  
  // Certificate 1 
  $wp_customize->add_setting( 'certificate_3' , array(
    'default'   => '',
    'transport' => 'refresh'
  ));
  
  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'certificate_3_control', array(
    'label'      => ('Your Certificate'),
    'section'    => 'education_skills_section',
    'settings'   => 'certificate_3',
    'width'      => '450'
  )));  

  // --- Employment History Section - Company 1  ---
  $wp_customize->add_section( 'employment_history_section_1' , array(
    'title'      =>  'Employment History', 
    'priority'   => 3,
    'description' => ''
  ));

    // Card - Title
    $wp_customize->add_setting( 'professional_experience_card_title_1' , array(
      'default'   => 'Project Manager at Company Inc.',
      'transport' => 'refresh'
    ));
    
    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'professional_experience_card_title_1_control', array(
      'label'      => ("Your most recent position and the company's name"),
      'section'    => 'employment_history_section_1',
      'settings'   => 'professional_experience_card_title_1',
      'type' => 'text'    
    )));

    // Card - Logo
    $wp_customize->add_setting( 'professional_experience_card_logo_1' , array(
      'default'   => '',
      'transport' => 'refresh'
    ));
    
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'professional_experience_card_logo_1_control', array(
      'label'      => ('Company Logo'),
      'section'    => 'employment_history_section_1',
      'settings'   => 'professional_experience_card_logo_1',
      'width'      => '450'
    )));  

    // Responsibilities Description
    $wp_customize->add_setting( 'professional_experience_card_responsibilities_1' , array(
    'default'   => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Repellendus ipsa sit provident accusamus quasi? Distinctio ipsum sed cumque labore officia officiis aperiam repellat modi minus, eius molestiae a. Atque tempore similique vel corrupti cupiditate sunt ea quae ratione fuga doloremque. Inventore qui in eligendi mollitia ex distinctio nesciunt sunt dicta?',
    'transport' => 'refresh'
  ));

  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'professional_experience_card_responsibilities_1_control', array(
    'label'      => ('Describe your main responsibilities.'),
    'section'    => 'employment_history_section_1',
    'settings'   => 'professional_experience_card_responsibilities_1',
    'type' => 'textarea'    
  )));

  // Project 1
  // Project - Title
  $wp_customize->add_setting( 'professional_experience_card_1_project_title_1' , array(
    'default'   => 'Project Title',
    'transport' => 'refresh'
  ));
  
  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'professional_experience_card_1_project_title_1_control', array(
    'label'      => ("Title of an interesting project."),
    'section'    => 'employment_history_section_1',
    'settings'   => 'professional_experience_card_1_project_title_1',
    'type' => 'text'    
  )));

  // Project Description
  $wp_customize->add_setting( 'professional_experience_card_1_project_description_1' , array(
    'default'   => 'Identified a need for A ... collaborated with B ... implemented C ... achieved D.',
    'transport' => 'refresh'
  ));

  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'professional_experience_card_1_project_description_1_control', array(
    'label'      => ('Describe your contributions to the project.'),
    'section'    => 'employment_history_section_1',
    'settings'   => 'professional_experience_card_1_project_description_1',
    'type' => 'textarea'    
  )));

  // Project Image 1 
  $wp_customize->add_setting( 'project_1' , array(
    'default'   => '',
    'transport' => 'refresh'
  ));
  
  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'project_1_control', array(
    'label'      => ('Project Image'),
    'section'    => 'employment_history_section_1',
    'settings'   => 'project_1',
    'width'      => '450'
  )));  

  // Project 2
  // Project - Title
  $wp_customize->add_setting( 'professional_experience_card_1_project_title_2' , array(
    'default'   => 'Project Title',
    'transport' => 'refresh'
  ));
  
  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'professional_experience_card_1_project_title_2_control', array(
    'label'      => ("Title of an interesting project."),
    'section'    => 'employment_history_section_1',
    'settings'   => 'professional_experience_card_1_project_title_2',
    'type' => 'text'    
  )));

  // Project Description
  $wp_customize->add_setting( 'professional_experience_card_1_project_description_2' , array(
    'default'   => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Alias numquam nobis qui, fuga exercitationem omnis id maxime, doloribus molestiae repellat non in tenetur! Nulla labore tempore tenetur saepe doloribus fugit rem odit quas dignissimos eaque, fuga ipsam beatae animi harum incidunt deserunt eius sed, impedit necessitatibus pariatur. Pariatur, illum eius!',
    'transport' => 'refresh'
  ));

  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'professional_experience_card_1_project_description_2_control', array(
    'label'      => ('Describe your contributions to the project.'),
    'section'    => 'employment_history_section_1',
    'settings'   => 'professional_experience_card_1_project_description_2',
    'type' => 'textarea'    
  )));

    // Project Image 2 
    $wp_customize->add_setting( 'project_2' , array(
      'default'   => '',
      'transport' => 'refresh'
    ));
    
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'project_2_control', array(
      'label'      => ('Project Image'),
      'section'    => 'employment_history_section_1',
      'settings'   => 'project_2',
      'width'      => '450'
    )));  
  
  // Project 3
  // Project - Title
  $wp_customize->add_setting( 'professional_experience_card_1_project_title_3' , array(
    'default'   => 'Project Title',
    'transport' => 'refresh'
  ));
  
  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'professional_experience_card_1_project_title_3_control', array(
    'label'      => ("Title of an interesting project."),
    'section'    => 'employment_history_section_1',
    'settings'   => 'professional_experience_card_1_project_title_3',
    'type' => 'text'    
  )));

  // Project Description
  $wp_customize->add_setting( 'professional_experience_card_1_project_description_3' , array(
    'default'   => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, placeat. Quia quasi sit ipsam voluptatibus repudiandae, eaque itaque mollitia a amet expedita omnis, laudantium dolorem tempora similique blanditiis iusto nihil ea nostrum accusantium! Quis dolores laudantium quos sequi vitae minus dolorem eum, fuga iste beatae voluptas eligendi labore qui debitis!',
    'transport' => 'refresh'
  ));

  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'professional_experience_card_1_project_description_3_control', array(
    'label'      => ('Describe your contributions to the project.'),
    'section'    => 'employment_history_section_1',
    'settings'   => 'professional_experience_card_1_project_description_3',
    'type' => 'textarea'    
  )));

  // Project Image 3 
  $wp_customize->add_setting( 'project_3' , array(
    'default'   => '',
    'transport' => 'refresh'
  ));
  
  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'project_3_control', array(
    'label'      => ('Project Image'),
    'section'    => 'employment_history_section_1',
    'settings'   => 'project_3',
    'width'      => '450'
  )));  

  // Colegue Testimonials - 1
  $wp_customize->add_setting( 'employer_testimonials_1_image_1' , array(
    'default'   => '',
    'transport' => 'refresh'
  ));

  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'employer_testimonials_1_image_1_control', array(
    'label'      => ('Colegue testimonial - Image'),
    'section'    => 'employment_history_section_1',
    'settings'   => 'employer_testimonials_1_image_1',
    'width'      => '450'
  )));

  // Colegue Testimonial - Quote 1
  $wp_customize->add_setting( 'employer_testimonials_1_quote_1' , array(
    'default'   => 'Testimonial',
    'transport' => 'refresh'
  ));
    
  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'employer_testimonials_1_quote_1_control', array(
    'label'      => ("Quote"),
    'section'    => 'employment_history_section_1',
    'settings'   => 'employer_testimonials_1_quote_1',
    'type' => 'text'    
  )));

   // Colegue Testimonial - Name
   $wp_customize->add_setting( 'employer_testimonials_1_referee_name_1' , array(
    'default'   => 'Testimonial',
    'transport' => 'refresh'
  ));
    
  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'employer_testimonials_1_referee_name_1_control', array(
    'label'      => ("Referee Name"),
    'section'    => 'employment_history_section_1',
    'settings'   => 'employer_testimonials_1_referee_name_1',
    'type' => 'text'    
  )));

  // Colegue Testimonials - 2
  $wp_customize->add_setting( 'employer_testimonials_1_image_2' , array(
    'default'   => '',
    'transport' => 'refresh'
  ));

  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'employer_testimonials_1_image_2_control', array(
    'label'      => ('Colegue testimonial - Image'),
    'section'    => 'employment_history_section_1',
    'settings'   => 'employer_testimonials_1_image_2',
    'width'      => '450'
  )));

  // Colegue Testimonial - Quote 2
  $wp_customize->add_setting( 'employer_testimonials_1_quote_2' , array(
    'default'   => 'Testimonial',
    'transport' => 'refresh'
  ));
    
  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'employer_testimonials_1_quote_2_control', array(
    'label'      => ("Quote"),
    'section'    => 'employment_history_section_1',
    'settings'   => 'employer_testimonials_1_quote_2',
    'type' => 'text'    
  )));

  // Colegue Testimonial - Name
  $wp_customize->add_setting( 'employer_testimonials_1_referee_name_2' , array(
    'default'   => 'Testimonial',
    'transport' => 'refresh'
  ));
    
  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'employer_testimonials_1_referee_name_2_control', array(
    'label'      => ("Referee Name"),
    'section'    => 'employment_history_section_1',
    'settings'   => 'employer_testimonials_1_referee_name_2',
    'type' => 'text'    
  )));

  // Colegue Testimonials - 3
  $wp_customize->add_setting( 'employer_testimonials_1_image_3' , array(
    'default'   => '',
    'transport' => 'refresh'
  ));

  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'employer_testimonials_1_image_3_control', array(
    'label'      => ('Colegue testimonial - Image'),
    'section'    => 'employment_history_section_1',
    'settings'   => 'employer_testimonials_1_image_3',
    'width'      => '450'
  )));  

  // Colegue Testimonial - Quote 3
  $wp_customize->add_setting( 'employer_testimonials_1_quote_3' , array(
    'default'   => 'Testimonial',
    'transport' => 'refresh'
  ));
    
  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'employer_testimonials_1_quote_3_control', array(
    'label'      => ("Quote"),
    'section'    => 'employment_history_section_1',
    'settings'   => 'employer_testimonials_1_quote_3',
    'type' => 'text'    
  )));

  // Colegue Testimonial - Name
  $wp_customize->add_setting( 'employer_testimonials_1_referee_name_3' , array(
    'default'   => 'Testimonial',
    'transport' => 'refresh'
  ));
    
  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'employer_testimonials_1_referee_name_3_control', array(
    'label'      => ("Referee Name"),
    'section'    => 'employment_history_section_1',
    'settings'   => 'employer_testimonials_1_referee_name_3',
    'type' => 'text'    
  )));

   // --- Employment History Section - Company 2  ---
   $wp_customize->add_section( 'employment_history_section_2' , array(
    'title'      =>  'Employment History continue', 
    'priority'   => 3,
    'description' => ''
  ));

    // Card - Title
    $wp_customize->add_setting( 'professional_experience_card_title_2' , array(
      'default'   => 'Project Manager at Company Inc.',
      'transport' => 'refresh'
    ));
    
    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'professional_experience_card_title_2_control', array(
      'label'      => ("Your most recent position and the company's name"),
      'section'    => 'employment_history_section_2',
      'settings'   => 'professional_experience_card_title_2',
      'type' => 'text'    
    )));

    // Card - Logo
    $wp_customize->add_setting( 'professional_experience_card_logo_2' , array(
      'default'   => '',
      'transport' => 'refresh'
    ));
  
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'professional_experience_card_logo_2_control', array(
      'label'      => ('Company Logo'),
      'section'    => 'employment_history_section_2',
      'settings'   => 'professional_experience_card_logo_2',
      'width'      => '450'
    )));      

    // Responsibilities Description
    $wp_customize->add_setting( 'professional_experience_card_responsibilities_2' , array(
    'default'   => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Magni, minima voluptate, eius laudantium hic voluptatem adipisci natus possimus ut, cupiditate reprehenderit reiciendis accusantium. Cupiditate nobis et ullam, inventore iure voluptatum impedit. Laboriosam vel dolores, quisquam deleniti consequatur non recusandae repudiandae dicta nemo eum autem aliquam omnis consequuntur blanditiis provident laborum.',
    'transport' => 'refresh'
    ));

  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'professional_experience_card_responsibilities_2_control', array(
    'label'      => ('Describe your main responsibilities.'),
    'section'    => 'employment_history_section_2',
    'settings'   => 'professional_experience_card_responsibilities_2',
    'type' => 'textarea'    
  )));

  // Project 1
  // Project - Title
  $wp_customize->add_setting( 'professional_experience_card_2_project_title_1' , array(
    'default'   => 'Project Title',
    'transport' => 'refresh'
  ));
  
  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'professional_experience_card_2_project_title_1_control', array(
    'label'      => ("Title of an interesting project."),
    'section'    => 'employment_history_section_2',
    'settings'   => 'professional_experience_card_2_project_title_1',
    'type' => 'text'    
  )));

  // Project Description
  $wp_customize->add_setting( 'professional_experience_card_2_project_description_1' , array(
    'default'   => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Odio perferendis ut blanditiis dicta consequuntur. Excepturi ex dolore odio reiciendis ab. Quaerat blanditiis obcaecati dicta inventore odit reiciendis, beatae reprehenderit omnis doloremque atque. Unde ducimus optio asperiores sequi. Aspernatur inventore illum deleniti ipsa laudantium cupiditate accusamus, debitis numquam ab beatae provident?',
    'transport' => 'refresh'
  ));

  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'professional_experience_card_2_project_description_1_control', array(
    'label'      => ('Describe your contributions to the project.'),
    'section'    => 'employment_history_section_2',
    'settings'   => 'professional_experience_card_2_project_description_1',
    'type' => 'textarea'    
  )));

   // Project Image 4 
   $wp_customize->add_setting( 'project_4' , array(
    'default'   => '',
    'transport' => 'refresh'
  ));
  
  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'project_4_control', array(
    'label'      => ('Project Image'),
    'section'    => 'employment_history_section_2',
    'settings'   => 'project_4',
    'width'      => '450'
  )));  

  // Project 2
  // Project - Title
  $wp_customize->add_setting( 'professional_experience_card_2_project_title_2' , array(
    'default'   => 'Project Title',
    'transport' => 'refresh'
  ));
  
  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'professional_experience_card_2_project_title_2_control', array(
    'label'      => ("Title of an interesting project."),
    'section'    => 'employment_history_section_2',
    'settings'   => 'professional_experience_card_2_project_title_2',
    'type' => 'text'    
  )));

  // Project Description
  $wp_customize->add_setting( 'professional_experience_card_2_project_description_2' , array(
    'default'   => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis culpa, necessitatibus adipisci possimus esse sed reprehenderit harum iure. Eveniet ea labore cum molestias ut vitae id et nobis! Temporibus eum illum necessitatibus, itaque est repellendus tempora laborum cumque sunt asperiores, quia quam accusantium tempore nesciunt neque, aliquam nemo ipsam perferendis?',
    'transport' => 'refresh'
  ));

  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'professional_experience_card_2_project_description_2_control', array(
    'label'      => ('Describe your contributions to the project.'),
    'section'    => 'employment_history_section_2',
    'settings'   => 'professional_experience_card_2_project_description_2',
    'type' => 'textarea'    
  )));

  
   // Project Image 5 
   $wp_customize->add_setting( 'project_5' , array(
    'default'   => '',
    'transport' => 'refresh'
  ));
  
  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'project_5_control', array(
    'label'      => ('Project Image'),
    'section'    => 'employment_history_section_2',
    'settings'   => 'project_5',
    'width'      => '450'
  )));  
  
  // Project 3
  // Project - Title
  $wp_customize->add_setting( 'professional_experience_card_2_project_title_3' , array(
    'default'   => 'Project Title',
    'transport' => 'refresh'
  ));
  
  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'professional_experience_card_2_project_title_3_control', array(
    'label'      => ("Title of an interesting project."),
    'section'    => 'employment_history_section_2',
    'settings'   => 'professional_experience_card_2_project_title_3',
    'type' => 'text'    
  )));

  // Project Description
  $wp_customize->add_setting( 'professional_experience_card_2_project_description_3' , array(
    'default'   => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis, unde ab! Illo amet similique ipsum optio reiciendis. Tenetur, asperiores ipsum. Eveniet non nesciunt vero labore iure repellat, libero voluptatem sapiente doloribus quaerat. Eius perferendis excepturi deleniti exercitationem labore magni, praesentium ut fugiat provident. Neque nostrum quae aspernatur dolores quibusdam saepe?',
    'transport' => 'refresh'
  ));

  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'professional_experience_card_2_project_description_3_control', array(
    'label'      => ('Describe your contributions to the project.'),
    'section'    => 'employment_history_section_2',
    'settings'   => 'professional_experience_card_2_project_description_3',
    'type' => 'textarea'    
  )));

  
   // Project Image 6 
   $wp_customize->add_setting( 'project_6' , array(
    'default'   => '',
    'transport' => 'refresh'
  ));
  
  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'project_6_control', array(
    'label'      => ('Project Image'),
    'section'    => 'employment_history_section_2',
    'settings'   => 'project_6',
    'width'      => '450'
  )));  

  // Colegue Testimonials - 2
  $wp_customize->add_setting( 'employer_testimonials_2_image_1' , array(
    'default'   => '',
    'transport' => 'refresh'
  ));

  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'employer_testimonials_2_image_1_control', array(
    'label'      => ('Colegue testimonial - Image'),
    'section'    => 'employment_history_section_2',
    'settings'   => 'employer_testimonials_2_image_1',
    'width'      => '450'
  )));

  // Colegue Testimonial - Quote 1
  $wp_customize->add_setting( 'employer_testimonials_2_quote_1' , array(
    'default'   => 'Testimonial',
    'transport' => 'refresh'
  ));
    
  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'employer_testimonials_2_quote_1_control', array(
    'label'      => ("Quote"),
    'section'    => 'employment_history_section_2',
    'settings'   => 'employer_testimonials_2_quote_1',
    'type' => 'text'    
  )));

   // Colegue Testimonial - Name
   $wp_customize->add_setting( 'employer_testimonials_2_referee_name_1' , array(
    'default'   => 'Testimonial',
    'transport' => 'refresh'
  ));
    
  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'employer_testimonials_2_referee_name_1_control', array(
    'label'      => ("Referee Name"),
    'section'    => 'employment_history_section_2',
    'settings'   => 'employer_testimonials_2_referee_name_1',
    'type' => 'text'    
  )));

  // Colegue Testimonials - 2
  $wp_customize->add_setting( 'employer_testimonials_2_image_2' , array(
    'default'   => '',
    'transport' => 'refresh'
  ));

  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'employer_testimonials_2_image_2_control', array(
    'label'      => ('Colegue testimonial - Image'),
    'section'    => 'employment_history_section_2',
    'settings'   => 'employer_testimonials_2_image_2',
    'width'      => '450'
  )));

  // Colegue Testimonial - Quote 2
  $wp_customize->add_setting( 'employer_testimonials_2_quote_2' , array(
    'default'   => 'Testimonial',
    'transport' => 'refresh'
  ));
    
  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'employer_testimonials_2_quote_2_control', array(
    'label'      => ("Quote"),
    'section'    => 'employment_history_section_2',
    'settings'   => 'employer_testimonials_2_quote_2',
    'type' => 'text'    
  )));

  // Colegue Testimonial - Name
  $wp_customize->add_setting( 'employer_testimonials_2_referee_name_2' , array(
    'default'   => 'Testimonial',
    'transport' => 'refresh'
  ));
    
  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'employer_testimonials_2_referee_name_2_control', array(
    'label'      => ("Referee Name"),
    'section'    => 'employment_history_section_2',
    'settings'   => 'employer_testimonials_2_referee_name_2',
    'type' => 'text'    
  )));

  // Colegue Testimonials - 3
  $wp_customize->add_setting( 'employer_testimonials_2_image_3' , array(
    'default'   => '',
    'transport' => 'refresh'
  ));

  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'employer_testimonials_2_image_3_control', array(
    'label'      => ('Colegue testimonial - Image'),
    'section'    => 'employment_history_section_2',
    'settings'   => 'employer_testimonials_2_image_3',
    'width'      => '450'
  )));  

  // Colegue Testimonial - Quote 3
  $wp_customize->add_setting( 'employer_testimonials_2_quote_3' , array(
    'default'   => 'Testimonial',
    'transport' => 'refresh'
  ));
    
  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'employer_testimonials_2_quote_3_control', array(
    'label'      => ("Quote"),
    'section'    => 'employment_history_section_2',
    'settings'   => 'employer_testimonials_2_quote_3',
    'type' => 'text'    
  )));

  // Colegue Testimonial - Name
  $wp_customize->add_setting( 'employer_testimonials_2_referee_name_3' , array(
    'default'   => 'Testimonial',
    'transport' => 'refresh'
  ));
    
  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'employer_testimonials_2_referee_name_3_control', array(
    'label'      => ("Referee Name"),
    'section'    => 'employment_history_section_2',
    'settings'   => 'employer_testimonials_2_referee_name_3',
    'type' => 'text'    
  )));

}
add_action( 'customize_register', 'theme_customize_register' );
